#!/usr/bin/env node
var cph = require('../')
var cmd = process.argv[2]
if (cmd === 'install') {
  cph.installPackHackTransformer()
} else {
  throw new Error('cra-pack-hack: command not found "' + cmd + '"')
}
