'use strict'

var fs = require('fs')
var path = require('path')
var findFileUp = require('find-file-up')

// init
var pkgFilename = findFileUp.sync('package.json', path.resolve(__dirname, '..'))
function log (msg) {
  console.log(log.format(msg))
}
if (!pkgFilename) {
  throw new Error('[cra-pack-hack] react-scripts project not found')
}
var pkg = require(pkgFilename)
log.format = function logFormat (msg) {
  return '[' + pkg.name + '] ' + msg
}
var projectDirname = path.dirname(pkgFilename)
var reactScriptsConfigDirname = path
  .resolve(projectDirname, 'node_modules', 'react-scripts', 'config')
  .replace(/\\/g, '/')
var WEBPACK_CONFIG_FILENAMES = [
  'webpack.config.dev.js',
  'webpack.config.prod.js'
].map(function getConfigFilenames (basename) {
  return path.resolve(reactScriptsConfigDirname, basename).replace(/\\/g, '/')
})

function transformReactScriptsConfig (wpConfigContentBuffer, filename) {
  var content = wpConfigContentBuffer.toString()
  if (content.match(__filename)) {
    log('transform hooks already installed (' + path.basename(filename) + ')')
    return content
  }
  return content
    .replace(/;/g, '')
    .replace(
      /(module\.exports\s*=)\s*([\S\s]*)/g,
      '$1 require("' + __filename + '").transform($2)' // eslint-disable-line
    )
}

function installPackHackTransformer () {
  log('applying react-scripts webpack modification hooks')
  WEBPACK_CONFIG_FILENAMES.map(function getConfigFileContents (filename) {
    return { filename, contentBuffer: fs.readFileSync(filename) }
  })
    .map(function transformConfigFileContent (meta) {
      return Object.assign(meta, {
        transformedContent: transformReactScriptsConfig(meta.contentBuffer, meta.filename)
      })
    })
    .forEach(function persistTransformedConfig (meta) {
      return fs.writeFileSync(meta.filename, meta.transformedContent)
    })
}

function transform (webpackConfig) {
  var hacks = pkg['cra-pack-hack']
  if (!hacks) {
    throw new Error(log.format('"cra-pack-hack" key not found. please add it'))
  }
  if (!hacks.length) {
    return console.warn(
      log.format('"cra-pack-hack" has no transforms. skipping')
    )
  }
  return hacks.reduce(function applyPackHacks (agg, hack) {
    var filename

    if (!path.extname(hack)) {
      // is node_module
      filename = hack
    } else {
      filename = path.isAbsolute(hack)
        ? hack
        : path.resolve(projectDirname, hack)
    }
    return require(filename)(agg)
  }, webpackConfig)
}

module.exports = {
  installPackHackTransformer: installPackHackTransformer,
  transform: transform,
  WEBPACK_CONFIG_FILENAMES
}
