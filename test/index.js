require('perish')
var ava = require('ava').default
var fixtures = require('./fixtures/')

ava.beforeEach(async t => {
  await fixtures.setup(t)
})
ava.afterEach(t => fixtures.teardown(t))

ava('calls pack-hack transformers', async t => {
  await fixtures.installSimpleTransformer(t)
  var packHack = fixtures.remote(t)
  packHack.installPackHackTransformer()
  var hackedConfig = require(packHack.WEBPACK_CONFIG_FILENAMES[0])
  t.truthy(hackedConfig.__PACK_HACK, 'hacks applied')
})
