var fs = require('fs-extra')
var path = require('path')
var os = require('os')
var bluebird = require('bluebird')
var randomstring = require('randomstring').generate
var execa = require('execa')

var PACK_HACK_DIRNAME = path.resolve(__dirname, '../..')

async function copyFromProjectToTest (testDirname, basenames) {
  await bluebird.map(basenames, basename => {
    var src = path.join(PACK_HACK_DIRNAME, basename)
    var dest = path.join(testDirname, basename)
    return fs.copy(src, dest)
  })
}

async function setup (t) {
  var testDirname = path.join(os.tmpdir(), randomstring())
  await fs.mkdirp(testDirname)
  await copyFromProjectToTest(testDirname, ['src', 'package.json'])
  var pkgFilename = path.join(testDirname, 'package.json')
  var pkg = require(pkgFilename)
  var rsVersion = pkg.devDependencies['react-scripts']
  delete pkg.devDependencies
  pkg.dependencies['react-scripts'] = rsVersion
  await fs.writeFile(pkgFilename, JSON.stringify(pkg, null, 2))
  await execa.shell('yarn', { cwd: testDirname }) // , stdio: 'ignore'
  t.context.testDirname = testDirname
}

async function teardown (t) {
  await fs.remove(t.context.testDirname)
}

async function installSimpleTransformer (t) {
  var dirname = t.context.testDirname
  var pkgFilename = path.join(dirname, 'package.json')
  var pkg = require(pkgFilename)
  var simpleBasename = 'simple-pack-hack.js'
  pkg['cra-pack-hack'] = [simpleBasename]
  await fs.writeFile(pkgFilename, JSON.stringify(pkg, null, 2))
  await fs.copy(path.join(__dirname, simpleBasename), path.join(dirname, simpleBasename))
}

function remote (t) {
  return require(path.join(t.context.testDirname, 'src/index.js'))
}

module.exports = {
  installSimpleTransformer,
  remote,
  setup,
  teardown
}
