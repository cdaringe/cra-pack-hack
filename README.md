# cra-pack-hack

an easy way to use `create-react-app` and customize the hidden webpack build configuration to your own liking.

[![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com) [![build badge](https://gitlab.com/cdaringe/cra-pack-hack/badges/master/build.svg)](https://gitlab.com/cdaringe/cra-pack-hack)

## install

`npm install --save-dev cra-pack-hack`

## usage

- update your package.json and add an `install` script
- add a `cra-pack-hack` field, and list webpack transformation files
  - entries without file type extensions are assumed to be `npm` modules
  - otherwise, those files are called

```js
// package.json
{
  "scripts": {
    "install": "cra-pack-hack install"
  }
  "cra-pack-hack": [
    "pack-hack-ts-loader", // use extensions from npm/node_modules
    "pack-hack-react-preact-swap",
    "my-webpack-hack.js" // or use files! relative entries are relative to your app root
  ]
}
```

```js
// my-webpack-hack.js
module.exports = function editWebpackConfig (webpackConfig) {
  // webpackConfig is the parsed JS webpack config from react-scrips.
  // modify it here **synchronously**, & return it.
  return Object.assign({}, webpackConfig, {
    ...my-webpack-customizations
  })
}
```

## why

you love [create-react-app](https://github.com/facebookincubator/create-react-app), but need just a tiny tweak to the build process.

## why not

the whole point of sticking with react-scripts is offloading the complexity of the build system (webpack), and getting rolling updates.  tweaking it implies that you want ownership of the build process!

## justification

if you **must** change the build used by react-scripts (create-react-app) you can either fork the project, or `react-scripts eject`.  **until now!**

- forking stinks. rolling updates come with a cost of you or your team pulling down the upstream project. further, **git conflicts are bound to follow**.
- ejecting stinks.  you get a ton of code pushed into your codebase & immediately onboard technical debt by absorbing all of that code.

your custom modifications are sure to break when you bump react-scripts. however, the amount of modifications should be _limited_, so hopefully you only need to patch a few lines in your transformer.
